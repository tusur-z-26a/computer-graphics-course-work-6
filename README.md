# Computer Graphics Course Work 6

Для разработки и сборки требуется [Node.js](https://nodejs.org/en/) ≥12.

Перед работой необходимо установить зависимости:

```bash
npm install
```

## Разработка (development server)

```bash
npm start
```

## Сборка

```bash
npm run build
```
