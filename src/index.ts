import { debounce } from 'lodash';
import * as THREE from 'three';
// @ts-ignore
import OrbitControls from 'three-orbitcontrols';
import './teapotBufferGeometry';

import 'normalize.css';
import './styles/main.css';

interface SetupControlParams {
  initialValue: {
    rotationSpeed: number;
  };
  onRotationSpeedChange: (value: number) => void;
  onResetCameraClick: () => void;
}

const getOctahedronInlineSphereRadius = (octahedronRadius: number): number => {
  const a = Math.sqrt(octahedronRadius ** 2 + octahedronRadius ** 2);

  return (a / 6) * Math.sqrt(6);
};

const setupControls = (params: SetupControlParams): void => {
  const formName = 'control-form';
  const form = document.forms.namedItem(formName);

  if (!form) {
    // eslint-disable-next-line no-console
    console.warn(
      `No control form found with name "${formName}", ` +
        'form setup will not proceed',
    );

    return;
  }

  const getItem = (name: string): Element | RadioNodeList => {
    const item = form.elements.namedItem(name);

    if (!item) {
      throw Error(`Could not find form item with name "${name}"`);
    }

    return item;
  };

  const debouncedOnRotationChange = debounce(params.onRotationSpeedChange, 500);

  const controls = {
    rotationSpeed: getItem('rotation-speed') as HTMLInputElement,
    resetCamera: getItem('reset-camera') as HTMLButtonElement,
  };

  controls.rotationSpeed.value = params.initialValue.rotationSpeed.toString();

  controls.rotationSpeed.addEventListener('change', e => {
    debouncedOnRotationChange(
      parseInt((e.target as HTMLInputElement).value, 10),
    );
  });

  controls.resetCamera.addEventListener('click', () => {
    params.onResetCameraClick();
  });
};

const onReady = (container: Element): void => {
  const scene = new THREE.Scene();
  const camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    50,
  );

  const canvas = document.createElement('canvas');
  const context = canvas.getContext('webgl2') as WebGLRenderingContext;
  const renderer = new THREE.WebGLRenderer({ canvas, context });
  // TODO: Use given container size
  renderer.setSize(window.innerWidth, window.innerHeight);
  container.appendChild(renderer.domElement);

  const controls = new OrbitControls(camera, renderer.domElement);
  controls.enableZoom = true;
  controls.enableKeys = true;

  const octahedronRadius = 1;
  const sphereRadius = getOctahedronInlineSphereRadius(octahedronRadius);

  const sceneShapes = {
    octahedron: new THREE.Mesh(
      new THREE.OctahedronGeometry(octahedronRadius, 0),
      new THREE.MeshPhongMaterial({
        color: 0x0000e0,
        transparent: true,
        opacity: 0.75,
      }),
    ),
    sphere: new THREE.Mesh(
      new THREE.SphereGeometry(sphereRadius, 0),
      new THREE.MeshPhongMaterial({ color: 0xcc1e58 }),
    ),
    teapot: new THREE.Mesh(
      // @ts-ignore
      new THREE.TeapotBufferGeometry(1, 0),
      new THREE.MeshPhongMaterial({ color: 0xe8dd8d }),
    ),
  };

  const composedShapes = [sceneShapes.octahedron, sceneShapes.sphere];

  sceneShapes.teapot.position.x = 0;
  sceneShapes.teapot.position.y = 0;

  composedShapes.forEach(shape => {
    /* eslint-disable no-param-reassign */
    shape.position.x = 0.5;
    shape.position.y = 0.5;
    /* eslint-enable no-param-reassign */
  });

  Object.values(sceneShapes).forEach(shape => scene.add(shape));

  const color = new THREE.Color('lightblue');

  scene.fog = new THREE.Fog(0xadd8e6, 1, 15);

  scene.background = color;

  const light = new THREE.DirectionalLight(0xffffff, 1);
  light.position.set(-1, 2, 4);
  scene.add(light);

  let rotationSpeedModifier = 10;

  const resetCamera = (): void => {
    camera.position.x = 5;
    camera.position.y = 5;
    camera.position.z = 7;
    camera.lookAt(0, 0, 0);
  };
  resetCamera();
  controls.update();

  const animate = (): void => {
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
    controls.update();

    const z = 5;

    composedShapes.forEach(object => {
      const t =
        ((Date.now() % 60000) / 60000) * Math.PI * rotationSpeedModifier;

      /* eslint-disable no-param-reassign */
      object.rotation.x += 0.005;
      object.rotation.y += 0.005;

      object.position.x = z * Math.sin(t);
      object.position.z = z * Math.cos(t);
      /* eslint-enable no-param-reassign */
    });
  };

  animate();

  setupControls({
    initialValue: {
      rotationSpeed: rotationSpeedModifier,
    },
    onRotationSpeedChange: value => {
      rotationSpeedModifier = value;
    },
    onResetCameraClick: () => {
      resetCamera();
    },
  });
};

const getContainer = (selector: string): Element => {
  const container = document.querySelector(selector);

  if (container === null) {
    throw Error(`Could not find container with selector "${selector}"`);
  }

  return container;
};

try {
  const container = getContainer('div#root');

  if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', () => onReady(container));
  } else {
    onReady(container);
  }
} catch (e) {
  document.body.innerText = e.message;
  throw e;
}
